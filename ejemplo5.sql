﻿DROP DATABASE alquilerMULTIVALUADO;
CREATE DATABASE alquilerMULTIVALUADO;

USE alquilerMULTIVALUADO;

CREATE OR REPLACE TABLE socios(
  cod_socio int AUTO_INCREMENT,
  nombre varchar(25),

  PRIMARY KEY(cod_socio)
);


CREATE OR REPLACE TABLE peliculas(
  cod_pelicula int AUTO_INCREMENT,
  titulo varchar(50),
  PRIMARY KEY(cod_pelicula)
);


CREATE OR REPLACE TABLE alquila(
  socio int,
  pelicula int,
  PRIMARY KEY(socio,pelicula),

  CONSTRAINT FKalquilasocio FOREIGN KEY(socio)
  REFERENCES socios(cod_socio),

  CONSTRAINT FKalquilapelicula FOREIGN KEY(pelicula)
  REFERENCES peliculas(cod_pelicula)
);


CREATE OR REPLACE TABLE fecha(
  codigoSocio int,
  codigoPelicula int,
  fecha date,
  PRIMARY KEY(codigoSocio,codigoPelicula),

  CONSTRAINT FKfechacodigoSociocodigoPelicula FOREIGN KEY(codigoSocio,codigoPelicula)
  REFERENCES alquila(socio,pelicula)
);


CREATE OR REPLACE TABLE telefonos(
  socio int,
  tfno varchar(10),
  PRIMARY KEY(socio,tfno),

  CONSTRAINT FKtelefonossocio FOREIGN KEY(socio)
  REFERENCES socios(cod_socio)
);


CREATE OR REPLACE TABLE emails(
  socio int,
  email varchar(50),
  PRIMARY KEY(socio,email),

  CONSTRAINT FKemailssocios FOREIGN KEY(socio)
  REFERENCES socios(cod_socio)
);